﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.ViewModel.Invoice
{
    public class OrderDetailInvoice
    {
        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerRegion { get; set; }
        public string CustomerPostalCode { get; set; }
        //public string CustomerCountry { get; set; }
        public string CustomerPhone { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeTitleOfCourtesy { get; set; }
        public string EmployeeAddress { get; set; }
        public string EmployeeCity { get; set; }
        public string EmployeeRegion { get; set; }
        public string EmployeePostalCode { get; set; }
        public string EmployeeCountry { get; set; }
        public string EmployeePhone { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime RequiredDate { get; set; }
        public DateTime ShippedDate { get; set; }
        public int ShipVia { get; set; }
        public string ShipperName { get; set; }
        public string ShipperPhone { get; set; }
        public int Freight { get; set; }
        public string ShipName { get; set; }
        public string ShipAddress { get; set; }
        public string ShipCity { get; set; }
        public string ShipRegion { get; set; }
        public string ShipPostalCode { get; set; }
        public string ShipCountry { get; set; }

        public List<InvoiceDetail> DetailInvoice { get; set; }
        public double SubTotal { get; set; }
        public double Tax { get; set; }
        public double Shipping { get; set; }
        public double Total { get; set; }
    }
}
