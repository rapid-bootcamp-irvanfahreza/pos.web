﻿using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    [Table("tbl_shippers")]
    public class ShippersEntity
    {
        [Key]
        [Column("id")]
        public int ShipperId { get; set; }

        [Column("company_name")]
        public string CompanyName { get; set;}

        [Column("phone")]
        public string Phone { get; set; }

        public ICollection<OrdersEntity> OrdersEntities { get; set; }

        public ShippersEntity(ShipperModel model)
        {
            CompanyName = model.CompanyName;
            Phone = model.Phone;
        }

        public ShippersEntity()
        {

        }
    }
}
