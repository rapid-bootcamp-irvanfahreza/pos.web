﻿using Microsoft.EntityFrameworkCore;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    [Table("tbl_order_details")]
    public class OrderDetailsEntity
    {
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [ForeignKey("OrdersEntity")]
        //[Column("order_id")]
        public int OrderId { get; set; }
        public OrdersEntity OrdersEntity { get; set; }

        [Required]
        [ForeignKey("ProductsEntity")]
        //[Column("product_id")]
        public int ProductId { get; set; }
        public ProductsEntity ProductsEntity { get; set; }

        [Required]
        [Column("unit_price")]
        public double UnitPrice { get; set; }

        [Required]
        [Column("quantity")]
        public int Quantity { get; set; }

        [Required]
        [Column("discount")]
        public double Discount { get; set; }

        public OrderDetailsEntity(OrderDetailsModel model)
        {
            Id = model.Id;
            OrderId = model.OrderId;
            ProductId = model.ProductId;
            UnitPrice = model.UnitPrice;
            Quantity = model.Quantity;
            Discount = model.Discount;
        }

        public OrderDetailsEntity()
        {

        }      
    }
}
