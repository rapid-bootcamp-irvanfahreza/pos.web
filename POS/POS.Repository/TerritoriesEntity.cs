﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    [Table("tbl_territories")]
    public class TerritoriesEntity
    {
        [Key]
        [Column("id")]
        public int Territoryid { get; set; }

        [Column("teritorry_description")]
        public string TerritoryDescription { get; set; }

        [Column("region_id")]
        public int RegionId { get; set; }
        public RegionEntity RegionEntity { get; set; }

        public ICollection<EmployeeTerritoriesEntity> EmployeeTerritoriesEntities { get; set; }
    }
}
