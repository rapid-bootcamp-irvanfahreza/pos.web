﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {

        }
        public DbSet<CategoryEntity> CategoryEntities => Set<CategoryEntity>();
        public DbSet<CustomersEntity> CustomersEntities => Set<CustomersEntity>();
        public DbSet<EmployeesEntity> EmployeesEntities => Set<EmployeesEntity>();
        public DbSet<EmployeeTerritoriesEntity> EmployeeTerritoriesEntities => Set<EmployeeTerritoriesEntity>();
        public DbSet<OrderDetailsEntity> OrderDetailsEntities => Set<OrderDetailsEntity>();
        public DbSet<OrdersEntity> OrdersEntities => Set<OrdersEntity>();
        public DbSet<ProductsEntity> ProductsEntities => Set<ProductsEntity>();
        public DbSet<RegionEntity> RegionEntities => Set<RegionEntity>();
        public DbSet<ShippersEntity> ShippersEntities => Set<ShippersEntity>();
        public DbSet<SuppliersEntity> SuppliersEntities => Set<SuppliersEntity>();
        public DbSet<TerritoriesEntity> TerritoriesEntities => Set<TerritoriesEntity>();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderDetailsEntity>()
                  .HasKey(m => new {m.Id, m.OrderId, m.ProductId });
        }
    }
}
