﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    [Table("tbl_employee_territories")]
    public class EmployeeTerritoriesEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("employee_id")]
        public int EmployeeId { get; set; }
        public EmployeesEntity EmployeesEntity { get; set; }

        [Column("territory_id")]
        public int TerritoryId { get; set; }
        public TerritoriesEntity TerritoriesEntity { get; set; }
    }
}
