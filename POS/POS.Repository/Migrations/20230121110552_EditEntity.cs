﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace POS.Repository.Migrations
{
    public partial class EditEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tbl_orders_tbl_customers_CustomersEntityCustomerId",
                table: "tbl_orders");

            migrationBuilder.DropIndex(
                name: "IX_tbl_orders_CustomersEntityCustomerId",
                table: "tbl_orders");

            migrationBuilder.DropColumn(
                name: "CustomersEntityCustomerId",
                table: "tbl_orders");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_orders_CustomerId",
                table: "tbl_orders",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_tbl_orders_tbl_customers_CustomerId",
                table: "tbl_orders",
                column: "CustomerId",
                principalTable: "tbl_customers",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tbl_orders_tbl_customers_CustomerId",
                table: "tbl_orders");

            migrationBuilder.DropIndex(
                name: "IX_tbl_orders_CustomerId",
                table: "tbl_orders");

            migrationBuilder.AddColumn<int>(
                name: "CustomersEntityCustomerId",
                table: "tbl_orders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_tbl_orders_CustomersEntityCustomerId",
                table: "tbl_orders",
                column: "CustomersEntityCustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_tbl_orders_tbl_customers_CustomersEntityCustomerId",
                table: "tbl_orders",
                column: "CustomersEntityCustomerId",
                principalTable: "tbl_customers",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
