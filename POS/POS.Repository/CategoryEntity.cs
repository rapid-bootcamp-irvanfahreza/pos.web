﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using POS.ViewModel;

namespace POS.Repository
{
    [Table("tbl_category")]
    public class CategoryEntity
    {
        [Key]
        [Column("id")]
        public int CategoryId { get; set; }

        [Required]
        [Column("category_name")]
        public string CategoryName { get; set; }

        [Required]
        [Column("description")]
        public string Description { get; set; }

        public virtual ICollection<ProductsEntity> ProductEntity { get; set; }

        public CategoryEntity(CategoryModel model)
        {
            CategoryId = model.CategoryId;
            CategoryName = model.CategoryName;
            Description = model.Description;
        }

        public CategoryEntity()
        {

        }
    }
}