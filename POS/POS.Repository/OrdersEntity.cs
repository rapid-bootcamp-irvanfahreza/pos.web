﻿using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    [Table("tbl_orders")]
    public class OrdersEntity
    {
        [Key]
        [Column("id")]
        public int OrderId { get; set; }

        [Required]
        //[Column("customer_id")]
        [ForeignKey("CustomersEntity")]
        public int CustomerId { get; set; }
        public CustomersEntity CustomersEntity { get; set; }

        [Required]
        //[Column("employee_id")]
        [ForeignKey("EmployeesEntity")]
        public int EmployeeId { get; set; }
        public EmployeesEntity EmployeesEntity { get; set; }

        [Required]
        [Column("order_date")]
        public DateTime OrderDate { get; set; }

        [Required]
        [Column("required_date")]
        public DateTime RequiredDate { get; set; }

        [Required]
        [Column("shipped_date")]
        public DateTime ShippedDate { get; set; }

        [Required]
        //[Column("ship_via")]
        [ForeignKey("ShippersEntity")]
        public int ShipVia { get; set; }
        public ShippersEntity ShippersEntity { get; set; }

        [Required]
        [Column("freight")]
        public int Freight { get; set; }
        
        [Required]
        [Column("ship_name")]
        public string ShipName { get; set; }

        [Required]
        [Column("ship_address")]
        public string ShipAddress { get; set; }

        [Required]
        [Column("ship_city")]
        public string ShipCity { get; set; }

        [Required]
        [Column("ship_region")]
        public string ShipRegion { get; set; }

        [Required]
        [Column("ship_postal_code")]
        public string ShipPostalCode { get; set; }

        [Required]
        [Column("ship_country")]
        public string ShipCountry { get; set; }

        //public ICollection<OrderDetailsEntity> OrderDetailsEntities { get; set; }
        public List<OrderDetailsEntity> OrderDetails { get; set; }

        //public OrdersEntity(OrderModel model)
        //{
        //    CustomerId = model.CustomerId;
        //    EmployeeId = model.EmployeeId;
        //    OrderDate = model.OrderDate;
        //    RequiredDate = model.RequiredDate;
        //    ShippedDate = model.ShippedDate;
        //    ShipVia = model.ShipVia;
        //    Freight = model.Freight;
        //    ShipName = model.ShipName;
        //    ShipAddress = model.ShipAddress;
        //    ShipCity = model.ShipCity;
        //    ShipRegion = model.ShipRegion;
        //    ShipPostalCode = model.ShipPostalCode;
        //    ShipCountry = model.ShipCountry;
        //}

        public OrdersEntity()
        {

        }
    }
}
