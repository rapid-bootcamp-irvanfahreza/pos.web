﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    [Table("tbl_region")]
    public class RegionEntity
    {
        [Key]
        [Column("id")]
        public int RegionId { get; set; }

        [Column("region_description")]
        public string RegionDescription { get; set; }

        public ICollection<TerritoriesEntity> TerritoriesEntities { get; set; }
    }
}
