﻿using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    [Table("tbl_products")]
    public class ProductsEntity
    {
        [Key]
        [Column("id")]
        public int ProductId { get; set; }

        [Required]
        [Column("product_name")]
        public string ProductName { get; set; }

        [Required]
        //[Column("supplier_id")]
        [ForeignKey("SuppliersEntity")]
        public int SupplierId { get; set; }
        public SuppliersEntity SuppliersEntity { get; set; }

        [Required]
        //[Column("category_id")]
        [ForeignKey("CategoryEntity")]
        public int CategoryId { get; set; }
        public virtual CategoryEntity CategoryEntity { get; set; }

        [Required]
        [Column("quantity_per_unit")]
        public string QuantityPerUnit { get; set; }

        [Required]
        [Column("unit_price")]
        public double UnitPrice { get; set; }

        [Required]
        [Column("units_in_stock")]
        public int UnitsInStock { get; set; }

        [Required]
        [Column("units_in_order")]
        public int UnitsInOrder { get; set; }

        [Required]
        [Column("reorder_level")]
        public int ReorderLevel { get; set; }

        [Required]
        [Column("discontinued")]
        public bool Discontinued { get; set; }

        public virtual ICollection<OrderDetailsEntity> OrderDetailsEntities { get; set; }

        public ProductsEntity(POS.ViewModel.ProductModel model)
        {            
            ProductName = model.ProductName;
            SupplierId = model.SupplierId;
            CategoryId = model.CategoryId;
            QuantityPerUnit = model.QuantityPerUnit;
            UnitPrice = model.UnitPrice;
            UnitsInStock = model.UnitsInStock;
            UnitsInOrder = model.UnitsInOrder;
            ReorderLevel = model.ReorderLevel;
            Discontinued = model.Discontinued;
        }

        public ProductsEntity()
        {
            Discontinued = false;
        }
    }
}
