﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly CategoryService _service;

        public CategoryController(ApplicationContext context)
        {
            _service = new CategoryService(context);
        }

        [HttpGet]
        public IActionResult List()
        {
            var Data = _service.GetEntities();
            return View(Data);
        }


        [HttpGet]
        public IActionResult Details(int? id)
        {
            var Data = _service.Find(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AddModal()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("CategoryName, Description")] CategoryModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Add(new CategoryEntity(request));
                return Redirect("List");
            }
            return View("Add", request);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var Data = _service.Find(id);
            return View(Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("CategoryId, CategoryName, Description")] CategoryModel request)
        {
            if (ModelState.IsValid)
            {
                //CategoryEntity categoryEntity = new CategoryEntity(category);
                //categoryEntity.CategoryId = category.CategoryId;
                _service.Update(request);
                return Redirect("List");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            //var entity = _service.Find(id);
            _service.Delete(id);
            //return View(entity);
            return Redirect("/Category/List");
        }
    }
}
