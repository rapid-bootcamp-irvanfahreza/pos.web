﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.web.Controllers
{
    public class OrderDetailsController : Controller
    {
        private readonly OrderDetailService _service;

        public OrderDetailsController(ApplicationContext context)
        {
            _service = new OrderDetailService(context);
        }

        [HttpGet]
        public IActionResult List()
        {
            var Data = _service.GetEntities();
            return View(Data);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var Data = _service.Find(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AddModal()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("OrderId, ProductId, UnitPrice, Quantity, Discount")] OrderDetailsModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Add(new OrderDetailsEntity(request));
                return Redirect("List");
            }
            return View("Add", request);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var Data = _service.Find(id);
            return View(Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("OrderId, ProductId, UnitPrice, Quantity, Discount")] OrderDetailsModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Update(request);
                return Redirect("List");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.Delete(id);
            return Redirect("/OrderDetails/List");
        }
    }
}
