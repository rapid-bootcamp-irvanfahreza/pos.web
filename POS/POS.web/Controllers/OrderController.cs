﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.web.Controllers
{
    public class OrderController : Controller
    {
        private readonly OrderService _serviceOrder;
        private readonly ProductsService _serviceProduct;
        private readonly CustomerService _serviceCustomer;
        private readonly EmployeeService _serviceEmployee;
        private readonly ShipperService _serviceShipper;

        public OrderController(ApplicationContext context)
        {
			_serviceOrder = new OrderService(context);
            _serviceCustomer = new CustomerService(context);
            _serviceEmployee = new EmployeeService(context);
            _serviceProduct = new ProductsService(context);
            _serviceShipper = new ShipperService(context);
        }

        [HttpGet]
        public IActionResult List()
        {
            var Data = _serviceOrder.GetEntities();
            return View(Data);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var Data = _serviceOrder.FindInvoice(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            ViewBag.Customer = new SelectList(_serviceCustomer.GetEntities(), "CustomerId", "ContactName");
            ViewBag.Product = new SelectList(_serviceProduct.GetEntities(), "ProductId", "ProductName");
            ViewBag.ProductPrice = new SelectList(_serviceProduct.GetEntities(), "ProductId", "UnitPrice");
            ViewBag.Employee = new SelectList(_serviceEmployee.GetEntities(), "EmployeeId", "FirstName");
            ViewBag.Shipper = new SelectList(_serviceShipper.GetEntities(), "ShipperId", "CompanyName");
            return View();
        }

        [HttpGet]
        public IActionResult AddModal()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry, OrderDetails")] OrderModel request)
        {
            if(ModelState.IsValid)
            {
				_serviceOrder.Add(request);
                return Redirect("List");
            }
            return View("Add", request);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            ViewBag.Customer = new SelectList(_serviceCustomer.GetEntities(), "CustomerId", "ContactName");
            ViewBag.Product = new SelectList(_serviceProduct.GetEntities(), "ProductId", "ProductName");
            ViewBag.ProductPrice = new SelectList(_serviceProduct.GetEntities(), "ProductId", "UnitPrice");
            ViewBag.Employee = new SelectList(_serviceEmployee.GetEntities(), "EmployeeId", "FirstName");
            ViewBag.Shipper = new SelectList(_serviceShipper.GetEntities(), "ShipperId", "CompanyName");
            var Data = _serviceOrder.Find(id);
            return View(Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("OrderId, CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry, OrderDetails")] OrderModel request)
        {
            if (ModelState.IsValid)
            {
				_serviceOrder.Update(request);
                return Redirect("List");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
			_serviceOrder.Delete(id);
            return Redirect("/Order/List");
        }
    }
}
