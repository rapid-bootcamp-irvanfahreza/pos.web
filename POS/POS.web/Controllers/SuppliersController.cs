﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.web.Controllers
{
    public class SuppliersController : Controller
    {
        private readonly SupplierService _service;

        public SuppliersController(ApplicationContext context)
        {
            _service = new SupplierService(context);
        }

        [HttpGet]
        public IActionResult List()
        {
            var Data = _service.GetEntities();
            return View(Data);
        }


        [HttpGet]
        public IActionResult Details(int? id)
        {
            var Data = _service.Find(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AddModal()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, HomePage")] SupplierModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Add(new SuppliersEntity(request));
                return Redirect("List");
            }
            return View("Add", request);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var Data = _service.Find(id);
            return View(Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("SupplierId, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, HomePage")] SupplierModel request)
        {
            if (ModelState.IsValid)
            {
                //SuppliersEntity suppliersEntity = new SuppliersEntity(supplier);
                //suppliersEntity.SupplierId = supplier.SupplierId;
                _service.Update(request);
                return Redirect("List");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            //var entity = _service.Find(id);
            _service.Delete(id);
            return Redirect("/Suppliers/List");
        }
    }
}
