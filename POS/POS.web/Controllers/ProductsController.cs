﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.web.Controllers
{
    public class ProductsController : Controller
    {
        private readonly ProductsService _serviceProduct;
        private readonly SupplierService _serviceSupplier;
        private readonly CategoryService _serviceCategory;

        public ProductsController(ApplicationContext context)
        {
            _serviceProduct = new ProductsService(context);
            _serviceSupplier = new SupplierService(context);
            _serviceCategory = new CategoryService(context);
        }

        [HttpGet]
        public IActionResult List()
        {
            var Data = _serviceProduct.GetEntities();
            return View(Data);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var Data = _serviceProduct.Find(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            //ViewBag.Product = new SelectList(_serviceProduct.GetEntities(), "ProductId", "ProductName");
            ViewBag.Category = new SelectList(_serviceCategory.GetEntities(), "CategoryId", "CategoryName");
            ViewBag.Supplier = new SelectList(_serviceSupplier.GetEntities(), "SupplierId", "CompanyName");

            return View();
        }

        [HttpGet]
        public IActionResult AddModal()
        {
            ViewBag.Category = new SelectList(_serviceCategory.GetEntities(), "CategoryId", "CategoryName");
            ViewBag.Supplier = new SelectList(_serviceSupplier.GetEntities(), "SupplierId", "CompanyName");
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("ProductName, SupplierId, CategoryId, QuantityPerUnit, UnitPrice, UnitsInStock, UnitsInOrder, ReorderLevel, Discontinued")] ProductModel request)
        {
            if (ModelState.IsValid)
            {
                _serviceProduct.Add(new ProductsEntity(request));
                return Redirect("List");
            }
            return View("Add", request);
            
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            ViewBag.Category = new SelectList(_serviceCategory.GetEntities(), "CategoryId", "CategoryName");
            ViewBag.Supplier = new SelectList(_serviceSupplier.GetEntities(), "SupplierId", "CompanyName");
            var Data = _serviceProduct.Find(id);
            return View(Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("ProductId, ProductName, SupplierId, CategoryId, QuantityPerUnit, UnitPrice, UnitsInStock, UnitsInOrder, ReorderLevel, Discontinued")] ProductModel request)
        {
            if (ModelState.IsValid)
            {
                //ProductsEntity productsEntity = new ProductsEntity(product);
                //productsEntity.ProductId = product.ProductId;
                _serviceProduct.Update(request);
                return Redirect("List");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            var entity = _serviceProduct.Find(id);
            _serviceProduct.Delete(id);
            //return View(entity);
            return Redirect("/Products/List");
        }

    }
}
