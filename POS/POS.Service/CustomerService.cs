﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class CustomerService
    {
        private readonly ApplicationContext _context;

        public CustomerService(ApplicationContext context)
        {
            _context = context;
        }

        public CustomerModel EntityToModel(CustomersEntity entity)
        {
            CustomerModel result = new CustomerModel();
            result.CustomerId = entity.CustomerId;
            result.CompanyName = entity.CompanyName;
            result.ContactName = entity.ContactName;
            result.ContactTitle = entity.ContactTitle;
            result.Address = entity.Address;
            result.City = entity.City;
            result.Region = entity.Region;
            result.PostalCode = entity.PostalCode;
            result.Country = entity.Country;
            result.Phone = entity.Phone;
            result.Fax = entity.Fax;

            return result;
        }

        public void ModelToEntity (CustomerModel model, CustomersEntity entity)
        {
            entity.CustomerId = model.CustomerId;
            entity.CompanyName = model.CompanyName;
            entity.ContactName = model.ContactName;
            entity.ContactTitle = model.ContactTitle;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.Region = model.Region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.Phone = model.Phone;
            entity.Fax = model.Fax;
        }

        public List<CustomersEntity> GetEntities()
        {
            return _context.CustomersEntities.ToList();
        }

        public CustomerModel Find(int? id)
        {
            var customer = _context.CustomersEntities.Find(id);
            return EntityToModel(customer);
        }

        public void Add(CustomersEntity request)
        {
            _context.CustomersEntities.Add(request);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var entity = _context.CustomersEntities.Find(id);
            _context.CustomersEntities.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(CustomerModel customer)
        {
            var entity = _context.CustomersEntities.Find(customer.CustomerId);
            ModelToEntity(customer, entity);
            _context.CustomersEntities.Update(entity);
            _context.SaveChanges();
        }
    }
}
