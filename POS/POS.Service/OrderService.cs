﻿using POS.Repository;
using POS.ViewModel;
using POS.ViewModel.Invoice;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class OrderService
    {
        private readonly ApplicationContext _context;
        //private readonly OrderDetailService _OrdeDetailService;

        public OrderService(ApplicationContext context)
        {
            _context = context;
        }

		private OrderDetailsModel EntityToModelOrdet(OrderDetailsEntity entity)
		{
			OrderDetailsModel result = new OrderDetailsModel();
			result.Id = entity.Id;
            result.OrderId = entity.OrderId;
            result.ProductId = entity.ProductId;
			result.UnitPrice = entity.UnitPrice;
			result.Quantity = entity.Quantity;
			result.Discount = entity.Discount;

			return result;
		}

		private OrderDetailsEntity ModelToEntityOrdet(OrderDetailsModel model)
		{
            OrderDetailsEntity entity = new OrderDetailsEntity();
            entity.Id = model.Id;
            entity.OrderId = model.OrderId;
			entity.ProductId = model.ProductId;
			entity.UnitPrice = model.UnitPrice;
			entity.Quantity = model.Quantity;
			entity.Discount = model.Discount;

            return entity;
		}

		private OrderModel EntityToModel(OrdersEntity entity)
        {
            var result = new OrderModel();
            result.OrderId = entity.OrderId;
            result.CustomerId = entity.CustomerId;
            result.EmployeeId= entity.EmployeeId;
            result.OrderDate = entity.OrderDate;
            result.RequiredDate = entity.RequiredDate;
            result.ShippedDate = entity.ShippedDate;
            result.ShipVia = entity.ShipVia;
            result.Freight = entity.Freight;
            result.ShipName = entity.ShipName;
            result.ShipAddress = entity.ShipAddress;
            result.ShipCity = entity.ShipCity;
            result.ShipRegion = entity.ShipRegion;
            result.ShipPostalCode = entity.ShipPostalCode;
            result.ShipCountry = entity.ShipCountry;
            result.OrderDetails = new List<OrderDetailsModel>();
            foreach (var item in entity.OrderDetails)
            {
                result.OrderDetails.Add(EntityToModelOrdet(item));
            }

            return result;
        }

        private OrdersEntity ModelToEntity (OrderModel model)
        {
            var entity = new OrdersEntity();
            entity.CustomerId = model.CustomerId;
            entity.EmployeeId = model.EmployeeId;
            entity.OrderDate = model.OrderDate;
            entity.RequiredDate = model.RequiredDate;
            entity.ShippedDate = model.ShippedDate;
            entity.ShipVia = model.ShipVia;
            entity.Freight = model.Freight;
            entity.ShipName = model.ShipName;
            entity.ShipAddress = model.ShipAddress;
            entity.ShipCity = model.ShipCity;
            entity.ShipRegion = model.ShipRegion;
            entity.ShipPostalCode = model.ShipPostalCode;
            entity.ShipCountry = model.ShipCountry;
            entity.OrderDetails = new List<OrderDetailsEntity>();
            foreach (var item in model.OrderDetails)
            {
                entity.OrderDetails.Add(ModelToEntityOrdet(item));
            }

            return entity;
        }

        private OrderDetailInvoice EntityToModelOrdetInvoice(OrdersEntity entity)
        {
            var shipper = _context.ShippersEntities.Find(entity.ShipVia);
            var customer = _context.CustomersEntities.Find(entity.CustomerId);
            var employee = _context.EmployeesEntities.Find(entity.EmployeeId);

            var invoice = new OrderDetailInvoice();
            invoice.OrderId = entity.OrderId;
            invoice.CustomerId = customer.CustomerId;
            invoice.ContactName = customer.ContactName;
            invoice.ContactTitle = customer.ContactTitle;
            invoice.CustomerAddress = customer.Address;
            invoice.CustomerCity = customer.City;
            invoice.CustomerRegion = customer.Region;
            invoice.CustomerPostalCode = customer.PostalCode;
            //invoice.CustomerCountry = customer.Country;
            invoice.CustomerPhone = customer.Phone;
            invoice.EmployeeFirstName = employee.FirstName;
            invoice.EmployeeTitleOfCourtesy = employee.TitleOfCourtesy;
            invoice.EmployeeAddress = employee.Address;
            invoice.EmployeeCity = employee.City;
            invoice.EmployeeRegion = employee.Region;
            invoice.EmployeePostalCode = employee.PostalCode;
            invoice.EmployeeCountry = employee.Country;
            invoice.EmployeePhone = employee.HomePhone;
            invoice.OrderDate = entity.OrderDate;
            invoice.RequiredDate = entity.RequiredDate;
            invoice.ShippedDate = entity.ShippedDate;
            invoice.ShipVia = shipper.ShipperId;
            invoice.ShipperName = shipper.CompanyName;
            invoice.ShipperPhone = shipper.Phone;
            invoice.Freight = entity.Freight;
            invoice.ShipName = entity.ShipName;
            invoice.ShipAddress = entity.ShipAddress;
            invoice.ShipCity = entity.ShipCity;
            invoice.ShipRegion = entity.ShipRegion;
            invoice.ShipPostalCode = entity.ShipPostalCode;
            invoice.ShipCountry = entity.ShipCountry;
            invoice.DetailInvoice = new List<InvoiceDetail>();

            foreach (var item in entity.OrderDetails)
            {
                invoice.DetailInvoice.Add(EntityToModelInvoiceDetail(item));
            }
            var subTotal = 0.0;
            foreach (var item in invoice.DetailInvoice)
            {
                item.SubTotal = (item.Quantity * item.UnitPrice) * (1 - item.Discount / 100);
                subTotal += item.SubTotal;
            }
            invoice.SubTotal = subTotal;
            invoice.Tax = subTotal * 0.1;
            invoice.Shipping = 0.0;
            invoice.Total = invoice.SubTotal + invoice.Tax + invoice.Shipping;

            return invoice;
        }

        private InvoiceDetail EntityToModelInvoiceDetail(OrderDetailsEntity entity)
        {
            var product = _context.ProductsEntities.Find(entity.ProductId);

            var invoice = new InvoiceDetail();
            invoice.Id = entity.Id;
            invoice.ProductId = product.ProductId;
            invoice.ProductName = product.ProductName;
            invoice.Quantity = entity.Quantity;
            invoice.UnitPrice = entity.UnitPrice;
            invoice.Discount = entity.Discount;

            return invoice;
        }

        public List<OrdersEntity> GetEntities()
        {
            return _context.OrdersEntities.ToList();
        }

        public OrderModel Find(int? id)
        {
            var order = _context.OrdersEntities.Find(id);
            var ordet = _context.OrderDetailsEntities.Where(a => a.OrderId == id);
            foreach (var item in ordet)
            {

            }
            return EntityToModel(order);
        }

        public void Add(OrderModel request)
        {
            var data = ModelToEntity(request);
            _context.OrdersEntities.Add(data);
            foreach (var item in data.OrderDetails)
            {
                item.OrderId = request.OrderId;
                _context.OrderDetailsEntities.Add(item);
            }
            _context.SaveChanges();
        }

        public OrderDetailInvoice FindInvoice(int? id)
        {
            var order = _context.OrdersEntities.Find(id);
            var ordet = _context.OrderDetailsEntities.Where(a => a.OrderId == id).ToList();
            order.OrderDetails = ordet;
            var orderInvoice = EntityToModelOrdetInvoice(order);
            return orderInvoice;

        }

        public void Delete(int? id)
        {
            var order = _context.OrdersEntities.Find(id);
            _context.OrdersEntities.Remove(order);
            var ordet = _context.OrderDetailsEntities.Where(a => a.Id == id);
            foreach (var item in ordet)
            {
                _context.OrderDetailsEntities.Remove(item);
            }

            _context.SaveChanges();
        }

		//public void Update(OrderModel order)
		//{
		//    var entity = _context.OrdersEntities.Find(order.OrderId);
		//    ModelToEntity(order, entity);
		//    _context.OrdersEntities.Update(entity);
		//    _context.SaveChanges();
		//}
		public void Update(OrderModel orderUpdate)
		{
			var entity = _context.OrdersEntities.Find(orderUpdate.OrderId);
            var ordet = _context.OrderDetailsEntities.Where(a => a.OrderId == orderUpdate.OrderId).ToList();

            var entityUpdate = ModelToEntity(orderUpdate);
            entity.CustomerId = entityUpdate.CustomerId;
            entity.EmployeeId = entityUpdate.EmployeeId;
            entity.OrderDate = entityUpdate.OrderDate;
            entity.RequiredDate = entityUpdate.RequiredDate;
            entity.ShippedDate = entityUpdate.ShippedDate;
            entity.ShipVia = entityUpdate.ShipVia;
            entity.ShipName = entityUpdate.ShipName;
            entity.ShipAddress = entityUpdate.ShipAddress;
            entity.ShipCity = entityUpdate.ShipCity;
            entity.ShipRegion = entityUpdate.ShipRegion;
            entity.ShipPostalCode = entityUpdate.ShipPostalCode;
            entity.ShipCountry = entityUpdate.ShipCountry;

			_context.OrdersEntities.Update(entity);
            foreach (var items in entity.OrderDetails)
            {
                items.OrderId = orderUpdate.OrderId;
                foreach (var item in ordet)
                {
                    if (items.ProductId == item.ProductId)
                    {
                        item.ProductId = items.ProductId;
                        item.Quantity = items.Quantity;
                        item.Discount = items.Discount;
                        item.UnitPrice = items.UnitPrice;

                        _context.OrderDetailsEntities.Update(item);
                    }
                }
            }
			_context.SaveChanges();
		}
	}
}
