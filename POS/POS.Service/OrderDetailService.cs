﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class OrderDetailService
    {
        private readonly ApplicationContext _context;
        public OrderDetailService(ApplicationContext context)
        {
            _context= context;
        }

        private OrderDetailsModel EntityToModel(OrderDetailsEntity entity)
        {
            OrderDetailsModel result = new OrderDetailsModel();
            result.Id = entity.Id;
            result.OrderId = entity.OrderId;
            result.ProductId = entity.ProductId;
            result.UnitPrice = entity.UnitPrice;
            result.Quantity = entity.Quantity;
            result.Discount = entity.Discount;

            return result;
        }

        public void ModelToEntity(OrderDetailsModel model, OrderDetailsEntity entity)
        {
            entity.Id = model.Id;
            entity.OrderId = model.OrderId;
            entity.ProductId = model.ProductId;
            entity.UnitPrice = model.UnitPrice;
            entity.Quantity = model.Quantity;
            entity.Discount = model.Discount;
        }

        public List<OrderDetailsEntity> GetEntities()
        {
            return _context.OrderDetailsEntities.ToList();
        }

        public OrderDetailsModel Find(int? id)
        {
            var ordet = _context.OrderDetailsEntities.Find(id);
            return EntityToModel(ordet);
        }

        public void Add(OrderDetailsEntity request)
        {
            _context.OrderDetailsEntities.Add(request);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var entity = _context.OrderDetailsEntities.Find(id);
            _context.OrderDetailsEntities.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(OrderDetailsModel orderDetails)
        {
            var entity = _context.OrderDetailsEntities.Find(orderDetails.Id);
            ModelToEntity(orderDetails, entity);
            _context.OrderDetailsEntities.Update(entity);
            _context.SaveChanges();
        }
    }
}
