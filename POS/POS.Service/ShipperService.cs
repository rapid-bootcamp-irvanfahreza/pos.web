﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class ShipperService
    {
        private readonly ApplicationContext _context;

        public ShipperService(ApplicationContext context)
        {
            _context = context;
        }

        private ShipperModel EntityToModel(ShippersEntity entity)
        {
            ShipperModel result = new ShipperModel();
            result.ShipperId = entity.ShipperId;
            result.CompanyName = entity.CompanyName;
            result.Phone = entity.Phone;

            return result;
        }

        private void ModelToEntity(ShipperModel model, ShippersEntity entity)
        {
            entity.CompanyName = model.CompanyName;
            entity.Phone = model.Phone;
        }

        public List<ShippersEntity> GetEntities()
        {
            return _context.ShippersEntities.ToList();
        }

        public ShipperModel Find(int? id)
        {
            var shipper = _context.ShippersEntities.Find(id);
            return EntityToModel(shipper);
        }

        public void Add(ShippersEntity request)
        {
            _context.ShippersEntities.Add(request);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var entity = _context.ShippersEntities.Find(id);
            _context.ShippersEntities.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(ShipperModel shipper)
        {
            var entity = _context.ShippersEntities.Find(shipper.ShipperId);
            ModelToEntity(shipper, entity);
            _context.ShippersEntities.Update(entity);
            _context.SaveChanges();
        }
    }
}
