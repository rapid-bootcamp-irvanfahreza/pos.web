﻿using Microsoft.EntityFrameworkCore;
using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationContext = POS.Repository.ApplicationContext;

namespace POS.Service
{
    public class SupplierService
    {
        private readonly ApplicationContext _context;
        public SupplierService(ApplicationContext context)
        {
            _context = context;
        }

        private SupplierModel EntityToModel(SuppliersEntity entity)
        {
            SupplierModel result = new SupplierModel();
            result.SupplierId = entity.SupplierId;
            result.CompanyName = entity.CompanyName;
            result.ContactName = entity.ContactName;
            result.ContactTitle = entity.ContactTitle;
            result.Address = entity.Address;
            result.City = entity.City;
            result.Region = entity.Region;
            result.PostalCode = entity.PostalCode;
            result.Country = entity.Country;
            result.Phone = entity.Phone;
            result.Fax = entity.Fax;
            result.HomePage = entity.HomePage;

            return result;
        }

        private void ModelToEntity(SupplierModel model, SuppliersEntity entity)
        {
            //entity.SupplierId = model.SupplierId;
            entity.CompanyName = model.CompanyName;
            entity.ContactName = model.ContactName;
            entity.ContactTitle = model.ContactTitle;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.Region = model.Region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.Phone = model.Phone;
            entity.Fax = model.Fax;
            entity.HomePage = model.HomePage;
        }

        public List<SuppliersEntity> GetEntities()
        {
            return _context.SuppliersEntities.ToList();
        }

        public SupplierModel Find(int? id)
        {
            var supplier = _context.SuppliersEntities.Find(id);
            return EntityToModel(supplier);
        }

        public void Add(SuppliersEntity request)
        {
            _context.SuppliersEntities.Add(request);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var entity = _context.SuppliersEntities.Find(id);
            _context.SuppliersEntities.Remove(entity);
            // commiting to database
            _context.SaveChanges();

        }
        public void Update(SupplierModel supplier)
        {
            //update ke entity
            var entity = _context.SuppliersEntities.Find(supplier.SupplierId);
            ModelToEntity(supplier, entity);
            _context.SuppliersEntities.Update(entity);
            //add to database
            _context.SaveChanges();
        }
    }

    
}
