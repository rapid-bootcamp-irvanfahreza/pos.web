﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationContext = POS.Repository.ApplicationContext;

namespace POS.Service
{
    public class ProductsService
    {
        private readonly ApplicationContext _context;
        public ProductsService(ApplicationContext context)
        {
            _context = context;
        }

        private ProductModel EntityToModel(ProductsEntity entity)
        {
            ProductModel result = new ProductModel();
            result.ProductId = entity.ProductId;
            result.ProductName = entity.ProductName;
            result.SupplierId = entity.SupplierId;
            result.CategoryId = entity.CategoryId;
            result.QuantityPerUnit = entity.QuantityPerUnit;
            result.UnitPrice = entity.UnitPrice;
            result.UnitsInStock = entity.UnitsInStock;
            result.UnitsInOrder = entity.UnitsInOrder;
            result.ReorderLevel = entity.ReorderLevel;
            result.Discontinued = entity.Discontinued;

            return result;
        }

        private void ModelToEntity(ProductModel model, ProductsEntity entity)
        {
            entity.ProductId = model.ProductId;
            entity.ProductName = model.ProductName;
            entity.SupplierId = model.SupplierId;
            entity.CategoryId = model.CategoryId;
            entity.QuantityPerUnit = model.QuantityPerUnit;
            entity.UnitPrice = model.UnitPrice;
            entity.UnitsInStock = model.UnitsInStock;
            entity.UnitsInOrder = model.UnitsInOrder;
            entity.ReorderLevel = model.ReorderLevel;
            entity.Discontinued = model.Discontinued;
        }

        public List<ProductsEntity> GetEntities()
        {
            return _context.ProductsEntities.ToList();
        }

        public ProductModel Find(int? id)
        {
            var products = _context.ProductsEntities.Find(id);
            return EntityToModel(products);
        }

        public void Add(ProductsEntity request)
        {
            _context.ProductsEntities.Add(request);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var entity = _context.ProductsEntities.Find(id);
            _context.ProductsEntities.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(ProductModel product)
        {
            var entity = _context.ProductsEntities.Find(product.ProductId);
            ModelToEntity(product, entity);
            _context.ProductsEntities.Update(entity);
            _context.SaveChanges();
        }

    }
}
