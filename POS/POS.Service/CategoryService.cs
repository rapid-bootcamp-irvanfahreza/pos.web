﻿using POS.Repository;
using POS.ViewModel;
using ApplicationContext = POS.Repository.ApplicationContext;

namespace POS.Service
{
    public class CategoryService
    {
        private readonly ApplicationContext _context;
        public CategoryService(ApplicationContext context)
        {
            _context = context;
        }

        private CategoryModel EntityToModel(CategoryEntity entity)
        {
            CategoryModel result = new CategoryModel();
            result.CategoryId = entity.CategoryId;
            result.CategoryName = entity.CategoryName;
            result.Description = entity.Description;

            return result;
        }

        private void ModelToEntity(CategoryModel model, CategoryEntity entity)
        {
            entity.CategoryId = model.CategoryId;
            entity.CategoryName = model.CategoryName;
            entity.Description = model.Description;

        }

        public List<CategoryEntity> GetEntities()
        {
            return _context.CategoryEntities.ToList();
        }

        public CategoryModel Find(int? id)
        {
            var category = _context.CategoryEntities.Find(id);
            return EntityToModel(category);
        }

        public void Add(CategoryEntity request)
        {
            _context.CategoryEntities.Add(request);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var entity = _context.CategoryEntities.Find(id);
            _context.CategoryEntities.Remove(entity);
            // commiting to database
            _context.SaveChanges();

        }

        public void Update(CategoryModel category)
        {
            //update ke entity
            var entity = _context.CategoryEntities.Find(category.CategoryId);
            ModelToEntity(category, entity);
            _context.CategoryEntities.Update(entity);
            //add to database
            _context.SaveChanges();
        }
    }
}