﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class EmployeeService
    {
        private readonly ApplicationContext _context;

        public EmployeeService(ApplicationContext context)
        {
            _context = context;
        }

        private EmployeeModel EntityToModel(EmployeesEntity entity)
        {
            EmployeeModel result = new EmployeeModel();
            result.EmployeeId = entity.EmployeeId;
            result.LastName = entity.LastName;
            result.FirstName = entity.FirstName;
            result.Title = entity.Title;
            result.TitleOfCourtesy = entity.TitleOfCourtesy;
            result.BirthDate = entity.BirthDate;
            result.HireDate = entity.HireDate;
            result.Address = entity.Address;
            result.City = entity.City;
            result.Region = entity.Region;
            result.PostalCode = entity.PostalCode;
            result.Country = entity.Country;
            result.HomePhone = entity.HomePhone;
            result.Extension = entity.Extension;
            result.Notes = entity.Notes;
            result.ReportsTo = entity.ReportsTo;

            return result;
        }

        private void ModelToEntity(EmployeeModel model, EmployeesEntity entity)
        {
            entity.EmployeeId = model.EmployeeId;
            entity.LastName = model.LastName;
            entity.FirstName = model.FirstName;
            entity.Title = model.Title;
            entity.TitleOfCourtesy = model.TitleOfCourtesy;
            entity.BirthDate = model.BirthDate;
            entity.HireDate = model.HireDate;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.Region = model.Region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.HomePhone = model.HomePhone;
            entity.Extension = model.Extension;
            entity.Notes = model.Notes;
            entity.ReportsTo = model.ReportsTo;

        }

        public List<EmployeesEntity> GetEntities()
        {
            return _context.EmployeesEntities.ToList();
        }

        public EmployeeModel Find(int? id)
        {
            var employee = _context.EmployeesEntities.Find(id);
            return EntityToModel(employee);
        }

        public void Add(EmployeesEntity request)
        {
            _context.EmployeesEntities.Add(request);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var entity = _context.EmployeesEntities.Find(id);
            _context.EmployeesEntities.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(EmployeeModel employee)
        {
            var entity = _context.EmployeesEntities.Find(employee.EmployeeId);
            ModelToEntity(employee, entity);
            _context.EmployeesEntities.Update(entity);
            _context.SaveChanges();
        }
    }
}
